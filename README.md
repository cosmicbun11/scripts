# Scripts

This is the actual script for my past and WIP videos. WIP scripts are in its separate branches and merged to master when the project is finished.

Reviews or fact checks welcome! Please read below.

# Proofreading

If you found errors or want to give suggestions on my script, you can create an Issue in the Issues tab. Describe the problems/improvements, and select an appropriate label. If your issue is about facts, cite sources as described below.

If you are familiar with Git and can create the suggestions for me, you can file a Merge Request for review.

## Fact checking

In general, every fact should have a source. This includes even mundane ones that you may take for granted (e.g., "Dogs bark"). You don't need to cite everything though. Cite facts that are considered important, i.e., it changes the main narrative. I keep a "Fact sheet" in fact-based projects (as apposed to experience-based projects) to track my findings.

Sources should be reputable. This usually means no blog/forum posts, no asking platforms (Quora, StackOverflow, etc.), no social media posts (Twitter, YouTube, Instagram, TikTok, Facebook, etc.), and [no Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Citing_Wikipedia#Problems_with_citing_Wikipedia). Books, or better yet, journal articles are your friend. News reports and official websites/announcements are acceptable.

Most of the time I work with topics that are time-sensitive. Please check if there is a newer study on the same topic before you use it.

Harvard style or APA style is preferred for references. If you don't know how to use them, simply give enough information about your sources so that I can find it by myself. Website links are fine.

### Citing Wikipedia

If you want to use something in Wikipedia, check the citation of the fact you're interested in, and use that source instead. If you don't have access to the source (e.g., can't find the book in library), cite *both* your Wikipedia article and the source. (e.g., book "Twilight (The Twilight Saga Book 1)" cited in Wikipedia article "Twilight (Meyer novel)")